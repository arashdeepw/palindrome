package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome("Anna"));
	}
	@Test
	public void testIsNotPalindrome() {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("Book"));
	}
	@Test
	public void testIsAlmostOutPalindrome() { //boundry out
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("Annb"));
	}
	@Test
	public void testIsAlmostInPalindrome() {//boundry in
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome("A"));
	}

}
